#!/usr/bin/env node

const markdown = require ('./src/build/markdown')
const public = require ("./src/build/public")
const templates = require ("./src/build/templates")
const watch = require ('./src/build/watch')

// const menus = require ('./src/build/menus')

const processMardownFile = filepath => {
   
  return new Promise ((success, error) => {

    markdown.convert (filepath)
        .then (templates.processAndSave)
        .then (success)
        .catch (error)
  })
}

exports.build = () => {
  public.clearDist ()
    .then (public.copyToDist)
    .then (watch.getMarkdownFilesList)
    .then (l => Promise.all (l.map (processMardownFile)))
    .then (() => console.log ("🐵 🍌🍌🍌"))
    .catch (e => console.log (e))
}

exports.buildOne = filename => {
  processMardownFile (filename)
}

exports.watch = () => {
  watch.markdown (processMardownFile)
}

exports.watchAndReloadTemplates = () => {
  watch.markdown (filepath => {
    templates.forceReload ()
    processMardownFile (filepath)
  })
}
