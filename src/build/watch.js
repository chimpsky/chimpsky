#!/usr/bin/env node

const gaze = require ('gaze')
const config = require ('./config').load ()
const path = require ('path')

exports.getMarkdownFilesList = () => {

  return new Promise ((success, error) => {
    const mainPath = path.dirname (require.main.filename)
    const srcFolder = path.join (mainPath, config.paths.markdown)
    const filePattern = srcFolder + '/**/*.md'

    gaze (filePattern, function (err, watcher) {
      if (err) {
        error (err)
        return
      }

      const files = this.relative ()
      this.close ()
      success (flattenFilesList (files))
    })
  })
}

exports.markdown = (callback) => {

  const mainPath = path.dirname (require.main.filename)
  const srcFolder = path.join (mainPath, config.paths.markdown)
  const filePattern = srcFolder + '/**/*.md'

  gaze (filePattern, function (err, watcher) {
    
    console.log (filePattern + " 👀  (ctrl-c to exit)")

    this.on ('changed', filepath => {
      callback (filepath)
    })

    this.on ('error', () => {
      console.log ('Error watching ' + filePattern)
      this.close ()
    })

    this.on ('nomatch', () => {
      console.log ('fyi, ' + filePattern + ' is empty')
      this.close ()
    })
  })
}

const flattenFilesList = filesObj => {
  const mainPath = path.dirname (require.main.filename)

  return Object.keys (filesObj)
          .reduce ((arr, k) => arr.concat (filesObj[k].map (f => k + f)), [])
          .filter (f => f.toLowerCase ().endsWith (".md"))
          .map (f => path.join (mainPath, f))
}

