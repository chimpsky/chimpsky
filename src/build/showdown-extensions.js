const showdownHighlight = require ("showdown-highlight")

const switchLinkExtensions = converter => {
  return [
    {
      type    : 'html', 
      regex   : '<a href="(.*).md">', 
      replace : '<a class="foog" href="$1.html">'
    }
  ]
}

const addUIKitClasses = converter => {
  return [
    {
      type    : 'html', 
      regex   : '<table>', 
      replace : '<table class="uk-table uk-table-striped uk-table-small uk-table-divider uk-width-auto">'
    },
    {
      type    : 'html', 
      regex   : '<ul>', 
      replace : '<ul class="uk-list uk-list-bullet">'
    }
  ]
}

exports.all = [switchLinkExtensions, addUIKitClasses, showdownHighlight]
