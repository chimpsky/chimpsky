#!/usr/bin/env node

const config = require ('./config').load ()
const fs = require ('fs')
const Handlebars = require ('handlebars')
const menus = require ('./menus')
const path = require ('path')
const chimpskyRoot = path.resolve (__dirname, "../", "../")

let templates

exports.processAndSave = data => {
 
  return new Promise ((success, error) => {

    loadTemplates ()
      .then (() => resolveOutputPath (data))
      .then (resolveEditUrl)
      .then (addMetadata)
      .then (compileTemplate)
      .then (saveHtmlFile)
      .then (success)
      .catch (error)

  })
}

exports.forceReload = () => templates = undefined

const resolveOutputPath = data => {

  return new Promise ((success, error) => {
    
    let destFilePath = getDestFilePathFromSrcFilePath (data.metadata.filepath)

    const folderPath = path.dirname (destFilePath)
    if (fs.existsSync (folderPath) == false) {
      fs.mkdirSync (folderPath, {recursive : true})
    }

    data.metadata.output_filepath = destFilePath
    success (data)
  })
}

const resolveEditUrl = data => {

  return new Promise ((success, error) => {

    const mainPath = path.dirname (require.main.filename)
    let edit_url = data.metadata.filepath

    if (edit_url.startsWith (mainPath)) {
      edit_url = edit_url.substr (mainPath.length)
    }

    data.metadata.edit_url = config.paths.edit_base + edit_url
    data.metadata.issues_url = config.paths.issues_url
    success (data) })
}

const addMetadata = data => {
  return new Promise ((success, error) => {
    data.metadata.menu = menus.getMenu (data.metadata.filepath)
    Object.assign (data.metadata, menus.getPageData (data.metadata.filepath))
    data.metadata.search_data = menus.getSearchData ()
    success (data)
  })
}

const compileTemplate = data => {
  
  return new Promise ((success, error) => {

    let template = templates[data.metadata.template]
    if (template == undefined) {
      error ('Template not found: ' + data.metadata.template)
      return
    }

    data.compiled = template (
      Object.assign (
        {},
        data.metadata,
        {content:data.content}
      )
    )

    success (data)
  })
}

const getDestFilePathFromSrcFilePath = filePath => {
  let destFilePath = path
                      .resolve (filePath)
                      .replace (path.resolve (config.paths.markdown), "")

  destFilePath = path.join (config.paths.dist, destFilePath)
  destFilePath = destFilePath.replace (new RegExp (".md" + '$'), ".html")

  return destFilePath
}

const loadTemplates = () => {

  return new Promise ((success, error) => {
    if (templates !== undefined) {
      success ()
      return
    }

    templates = {}
    fs.readdirSync (path.resolve (chimpskyRoot, config.paths.templates))
      .filter (file => file.endsWith ("handlebars"))
      .map (file => path.join (chimpskyRoot, config.paths.templates, file))
      .filter (file => fs.existsSync (file))
      .forEach (file => {
        const slug = path.basename (file, '.handlebars')
        const template = fs.readFileSync (file, 'utf8')
        templates[slug] = Handlebars.compile (template)
      })
    
    success ()
  })
}

const saveHtmlFile = data => {
  
  return new Promise ((success, error) => {
    
    fs.writeFileSync (data.metadata.output_filepath, data.compiled)
    console.log (data.metadata.output_filepath + "✨")
    success ()
  })
}
