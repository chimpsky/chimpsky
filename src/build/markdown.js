#!/usr/bin/env node

const fs = require ("fs")
const metadataParser = require('markdown-yaml-metadata-parser')
const path = require ("path")
const showdown = require ('showdown')
const showdownExtensions = require ('./showdown-extensions')
const config = require ('./config').load ()
const converter = new showdown.Converter ({tables : true, extensions : showdownExtensions.all})

exports.convert = filepath => {
  return new Promise ((success, error) => {
    resolveFilepath (filepath)
      .then (loadMarkdown)
      .then (filecontents => mergeMetadata (filecontents, filepath))
      .then (convertToHTML)
      .then (success)
      .catch (error)
  
  })
}

const resolveFilepath = filepath => {
  
  return new Promise ((success, error) => {
    if (fs.existsSync (filepath)) {
      success (filepath)
      return
    }
    
    filepath = path.join (config.paths.markdown, filepath)
    if (fs.existsSync (filepath)) {
      success (filepath)
      return
    }

    error (filepath + " not found")
  })
}

const loadMarkdown = filepath => {

  return new Promise ((success, error) => {
    const buf = fs.readFileSync (filepath)
    success (buf.toString ())
  })
}

const mergeMetadata = (fileContents, filepath) => {

  return new Promise ((success, error) => {

    const fileObj = metadataParser (fileContents)
    fileObj.metadata = Object.assign ({}, config.metadata, fileObj.metadata, {filepath})
    success (fileObj)
  })
}

const convertToHTML = fileObj => {

  return new Promise ((success, error) => {
    fileObj.content = converter.makeHtml (fileObj.content)
    success (fileObj)
  })
}
