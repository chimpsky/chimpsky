#!/usr/bin/env node

const fs = require ("fs")
const ncp = require ('ncp').ncp
const config = require ('./config').load ()
const path = require ('path')

const project_root = path.dirname (require.main.filename)
const public_path = path.resolve (project_root, config.paths.public)
const dist_path = path.resolve (project_root, config.paths.dist)

ncp.limit = 16

exports.clearDist = () => {
  return new Promise ((success, error) => {
    recursiveDistClearout (dist_path)
    success ()
  })
}

const recursiveDistClearout = function (path) {
  if (fs.existsSync (path)) {
    fs.readdirSync (path).forEach (function (file, index) {
      var curPath = path + "/" + file
      if (fs.lstatSync (curPath).isDirectory()) {
        recursiveDistClearout (curPath)
      } else {
        if (! curPath.endsWith ("app.js")) {
          fs.unlinkSync (curPath);
        }
      }
    })

    if (path !== dist_path) {
      fs.rmdirSync (path)
    }
  }
}

exports.copyToDist = () => {
  return new Promise ((success, error) => {
    
    ncp (public_path, dist_path, err => {
      if (err) {
        return error (err)
      }

      success ()
    })
  })
}
