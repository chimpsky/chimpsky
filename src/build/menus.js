'use strict'

const path = require ('path')
const fs = require ('fs')
const yaml = require ('js-yaml')
const config = require ('./config')

let data_project
let data_flat
let previous_url

exports.getMenu = current_file_path => {
  load ()
  
  const menu_titles = data_flat.map (m => m.menu_title).filter (unique)

  const menu = menu_titles.map (title => {

    const submenu_titles = data_flat
                            .filter (m => m.menu_title == title)
                            .map (m=>m.submenu_title)
                            .filter (unique)

    const submenus = submenu_titles.map (submenu_title => {

      const pages = data_flat
                      .filter (p => p.menu_title == title)
                      .filter (p => p.submenu_title == submenu_title)
                      .map (p => {
                        const active = current_file_path.endsWith (p.md_path)
                        
                        return {
                          title : p.title,
                          url : p.url,
                          class : active ? "uk-active" : "",
                          next_title : p.next_title,
                          next_url : p.next_url,
                          previous_title : p.previous_url,
                          previous_url : p.previous_url,
                          menu_title : p.menu_title,
                          submenu_title : p.submenu_title,
                          active
                        }
                      })

      const active = pages.some (p => p.active)
      const firstPage = pages.length > 0 ? pages[0] : false

      return {
        title : submenu_title == "" ? false : submenu_title,
        class : active ? "uk-scroll-to-me" : "",
        pages,
        active,
        url : firstPage ? firstPage.url : "#"
      }
    })

    const active = submenus.some (s => s.active)
    const firstPage = submenus.length > 0 ? submenus[0] : false

    return {
      title,
      slug: slugify (title),
      class : active ? "uk-open uk-active" : "",
      submenus,
      url : firstPage ? firstPage.url : "#"
    }
  })

  // console.dir (menu, { depth: null })

  return menu
}

exports.getPageData = current_file_path => {
  load ()
  const page = data_flat.find (p => current_file_path.endsWith (p.md_path))

  return {
    previous_url : page ? page.previous_url : false,
    previous_title : page ? page.previous_title : false,
    next_url : page ? page.next_url : false,
    next_title : page ? page.next_title : false,
    menu_title : page ? page.menu_title : false,
    submenu_title : page ? page.submenu_title : false,
    page_title : page ? page.title : false,
    html_title : page ? page.html_title : false,
  }
}

exports.getSearchData = () => {

  const data = data_flat.map (p => {
    return {
      title: p.menu_title + " / " + p.submenu_title+" / " + p.title,
      url: p.url
    }
  })

  return JSON.stringify (data)
}

const load = () => {
  return new Promise ((success, error) => {
    if (data_flat !== undefined) {
      return success ()
    }

    const config = require ('./config').load ()
    const project_root = path.dirname (require.main.filename)
    const menu_data_path = path.join (project_root, config.paths.menus)

    if (! fs.existsSync (menu_data_path)) {
      return error ("Menu file '"+menu_data_path+"' not found")
    }

    try {
      data_project = yaml.safeLoad (fs.readFileSync (menu_data_path, 'utf8'));
    } catch (e) {
      return error (e)
    }

    // console.dir (data_project, { depth: null })

    data_flat = flattenDataTree ()
    setPaginationButtonProperties (data_flat)
    // console.dir (data_flat, { depth: null })

    success ()
  })
}

const unique = (value, index , self) => self.indexOf (value) === index

const flattenDataTree = () => {

  if (! data_project) {
    return []
  }

  const flat_data = []
  const menu_titles = Object.keys (data_project)
  
  menu_titles.forEach (menu_title => {

    const menuArray = data_project [menu_title]

    menuArray.forEach (menuItem => {
      const is_string = typeof menuItem === 'string' || menuItem instanceof String

      if (is_string) {
        const page_title = menuItem
        flat_data.push (getPageObj (menu_title, "", page_title))
        return
      }

      const submenuObj = menuItem
      const submenuKeys = Object.keys (submenuObj)
      if (submenuKeys.length !== 1) {
        return
      }

      const submenu_title = submenuKeys[0]
      const pagesArray = submenuObj[submenu_title]

      pagesArray.forEach (page_title => {
        flat_data.push (getPageObj (menu_title, submenu_title, page_title))
      })
    })
  })

  return flat_data
}

const getPageObj = (menu_title, submenu_title, page_title) =>  {

  let file_path  = slugify (menu_title) + "/" 
  let html_title  = config.getProjectName () + " > " + menu_title + " > " 
  if (submenu_title !== "") {
    file_path += slugify (submenu_title) + "/" 
    html_title += submenu_title + " > " 
  }
  file_path += slugify (page_title)
  html_title += page_title

  const md_path = "/"+file_path+".md"
  const url = "/"+file_path+".html"

  const pagObj = {
    menu_title,
    submenu_title,
    title: page_title,
    md_path,
    url,
    html_title,
    previous_url
  }

  previous_url = url

  return pagObj
}

const setPaginationButtonProperties = () => {
  data_flat.forEach (page => {
    const nextPage = data_flat.find (p => p.previous_url == page.url)    
    page.next_title = nextPage ? nextPage.title : false
    page.next_url = nextPage ? nextPage.url : false
    
    const previousPage = data_flat.find (p => p.next_url == page.url)
    page.previous_title = previousPage ? previousPage.title : false
    page.previous_url = previousPage ? previousPage.url : false
  })
}

const slugify = text => {
  return text.toString ().toLowerCase ()
    .replace (/\s+/g, '-')
    .replace (/[^\w\-]+/g, '')
    .replace (/\-\-+/g, '-')
    .replace (/^-+/, '')
    .replace (/-+$/, '')
}
