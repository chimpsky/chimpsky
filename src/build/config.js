#!/usr/bin/env node

const fs = require ("fs")
const path = require ('path')
const appDir = path.dirname (require.main.filename)
let config = false

const paths = {
  "src" : "./src",
  "markdown" : "./docs",
  "menus" : "./menu.yaml",
  "public" : "./public",
  "templates" : "./src/templates",
  "dist" : "./dist",
  "edit_base" : "https://gitlab.com/my-group/my-project/blob/master",
  "issues_url" : "https://gitlab.com/my-group/my-project/issues/new"
}

const cache_buster = Date.now ()

const metadata = {
  "template" : "default",
  cache_buster
}

exports.load = () => {
  if (config !== false) {
    return config
  }

  config = {paths, metadata}
  
  if (fs.existsSync (appDir+"/chimpsky.config.js")) {
    const overrideConfig = require (appDir+"/chimpsky.config")

    Object.assign (config.paths, overrideConfig.paths)
    Object.assign (config.metadata, overrideConfig.metadata)
  }

  return config
}

exports.getProjectName = () => {
  const projectPackageJsonPath = appDir + '/package.json'
  if (! fs.existsSync (projectPackageJsonPath)) {
    return "My Project"
  }

  const pjson = require (projectPackageJsonPath)

  if (! pjson.name) {
    return "My Project"
  }

  return pjson.name
}