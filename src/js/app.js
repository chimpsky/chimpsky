'use strict'

import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'

UIkit.use (Icons)

var Suggestions = require ('suggestions')

var ready = function (fn) {
  if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
    fn ()
  } else {
    document.addEventListener ('DOMContentLoaded', fn)
  }
}

var setupSearch = function () {
  var input = document.querySelector ('.uk-search-input')
  var typeahead = new Suggestions (input, JSON.parse (input.getAttribute ("data")))
  typeahead.getItemValue = item => item.title
  input.addEventListener ('change', () => window.location = typeahead.selected.url)
}

ready (setupSearch)
