# Chimpsky

A Markdown 2 HTML library by [Toru Interactive](https://www.toruinteractive.com)

# Using in a project

```sh
npm install chimpsky --S
```

## Overriding settings 

create a file called `chimpsky.config.js` in the root of your project and override any of the following

```js
const paths = {
  "src" : "./src",
  "markdown" : "./docs",
  "menus" : "./docs/menus",
  "public" : "./public",
  "templates" : "./src/templates",
  "dist" : "./dist",
  "edit_base" : "https://gitlab.com/my-group/my-project/blob/master",
  "issues_url" : "https://gitlab.com/my-group/my-project/issues/new"
}

const metadata = {
  "template" : "default",
  "title" : "My Web Page" 
}
```

then add a couple of utility scripts to the root of your project...

`./build.js`

```js 
#!/usr/bin/env node
const chimpsky = require ('chimpsky')
chimpsky.build ()
````

`./watch.js`

```js 
#!/usr/bin/env node
const chimpsky = require ('chimpsky')
chimpsky.watch ()
````

`./watch-reload-templates.js`

```js 
#!/usr/bin/env node
const chimpsky = require ('chimpsky')
chimpsky.watchAndReloadTemplates ()
````

You can then run `node build` and `node watch`

# Developing

How to work on a local copy of chimpsky in your project. First clone your project into your Vagrant box...

 * make a `/app/tmp` folder in your vagrant box
 * make sure `/tmp` is .gitignored in your project
 * Clone chimpsky into `/app/tmp/chimpsky`

then npm link your local version of chimpsky to your project root...

```sh
vagrant ssh;
cd /app/tmp/chimpsky;
sudo npm link;
cd /app
sudo npm link chimpsky;
```

Then you can work on the files in your project's `/tmp/chimpsky` folder and run them directly from your project without publishing. 

# Publishing

change the version number in `./tmp/chimpsky/package.json` & commit your change, then publish...

```sh
vagrant ssh;
cd /app/tmp/chimpsky;
npm adduser; # use Sean's NPM login
npm publish --access public;
```

goto [chimpsky's NPM page](https://www.npmjs.com/package/chimpsky) to see if it's published. You should see your new version number on the right. 

